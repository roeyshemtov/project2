#pragma once
using namespace std;
#include "Monomial.h"
#include <iostream>

class Polynomial
{
private:
	Monomial ** poly;
	int size;
	bool isEmpty();
	void addPoly(Polynomial &p1, Polynomial &p2);
public:
	Polynomial();
	void print();
	void add(Monomial m);
	~Polynomial();
	friend ostream & operator << (ostream &out, Polynomial &p);
	friend ostream & operator << (ostream &out, Polynomial *p);
	friend istream & operator >> (istream &in, Polynomial &p);
	Polynomial& operator+= (const Monomial & m);
	bool operator== (const Monomial& m);
	bool operator!= (const Monomial& m);
	int operator() (int num);
	int operator[](int degree);
	Polynomial* operator+ (Polynomial & p);
	Polynomial* operator- ();
	void operator= (const Polynomial & poly);
};