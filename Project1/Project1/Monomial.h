#pragma once
#include <iostream>

using namespace std;

class Monomial
{
private:
	int coefficient;
	int degree;
	static int count;
public:
	Monomial();
	Monomial(int coefficient);
	Monomial(int coefficient, const int degree);
	Monomial(const Monomial &m);
	static int getNumberOfMonomials();
	static void setMonomial(Monomial& m, string str);
	void print();
	bool add(Monomial &m);
	~Monomial();
	friend class Polynomial;
	friend ostream & operator << (ostream &out, Monomial &m);
	friend ostream & operator << (ostream &out, Monomial *m);
	friend istream & operator >> (istream &in, Monomial &m);
	Monomial* operator+ (const Monomial & m);
	Monomial* operator- (const Monomial & m);
	Monomial* operator- ();
	Monomial* operator* (const Monomial & m);
	Monomial& operator*= (int num);
	Monomial& operator+= (const Monomial & m);
	Monomial& operator-= (const Monomial & m);
	int operator() (int num);
	bool operator== (const int num);
	bool operator!= (const int num);
	void operator= (const Monomial & m);
};

