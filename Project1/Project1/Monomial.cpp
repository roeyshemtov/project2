#include "Monomial.h"
#include <string>
#include <math.h>
using namespace std;

int Monomial::count = 0; // count's initialization

Monomial::Monomial()
{
	this->coefficient = 1;
	this->degree = 0;
	count++;
}

Monomial::Monomial(int coefficient)
{
	this->coefficient = coefficient;
	this->degree = 0;
}

Monomial::Monomial(int coefficient, const int degree)
{
	this->coefficient = coefficient;
	this->degree = degree;
}

Monomial::Monomial(const Monomial & m)
{
	this->coefficient = m.coefficient;
	this->degree = m.degree;
	count++;
}

int Monomial::getNumberOfMonomials()
{
	return count;
}

void Monomial::setMonomial(Monomial & m, string str)
{
	if (str.find("x") != std::string::npos) {
		if (str.find("^") != std::string::npos)
			m.degree = atoi(str.substr(str.find("^") + 1).c_str());
		else
			m.degree = 1;
		if (str.find("x") != 0)
			m.coefficient = atoi(str.substr(0, str.find("x")).c_str());
	}
	else {
		m.coefficient = atoi(str.c_str());
		m.degree = 0;
	}
}

void Monomial::print()
{
	if (this->coefficient == 0) {
		cout << "0";
	}
	else if (this->degree == 0) {
		cout << this->coefficient;
	}
	else if (this->coefficient == 1) {
		if (this->degree == 1)
			cout << "x";
		else cout << "x^" << this->degree;
	}
	else if (this->degree == 1)
		cout << this->coefficient << "*x";
	else cout << this->coefficient << "*x^" << this->degree;
}

bool Monomial::add(Monomial &m)
{
	if (this->degree != m.degree)
		return false;
	this->coefficient += m.coefficient;
	return true;
}

Monomial::~Monomial()
{
	count--;
}

Monomial* Monomial::operator+(const Monomial & m)
{
	if (this->degree == m.degree)
		return new Monomial(this->coefficient + m.coefficient, this->degree);
	return this;
}

Monomial * Monomial::operator-(const Monomial & m)
{
	if (this->degree == m.degree)
		return new Monomial(this->coefficient - m.coefficient, this->degree);
	return this;
}

Monomial * Monomial::operator-()
{
	return new Monomial(-1 * this->coefficient, this->degree);
}

int Monomial::operator()(int num)
{
	return (int)(this->coefficient * (pow(num, this->degree)));
}

Monomial * Monomial::operator*(const Monomial & m)
{
	return new Monomial(this->coefficient*m.coefficient, this->degree + m.degree);
}

Monomial & Monomial::operator*=(int num)
{
	this->coefficient *= num;
	return *this;
}

Monomial & Monomial::operator+=(const Monomial & m)
{
	if (this->degree == m.degree)
		this->coefficient += m.coefficient;
	return *this;
}

bool Monomial::operator== (const int num)
{
	return this->degree == 0 && this->coefficient == num;
}

bool Monomial::operator!= (const int num)
{
	return !this->operator==(num);
}

void Monomial::operator=(const Monomial & m)
{
	this->degree = m.degree;
	this->coefficient = m.coefficient;
}

Monomial & Monomial::operator-=(const Monomial & m)
{
	if (this->degree == m.degree)
		this->coefficient -= m.coefficient;
	return *this;
}

ostream & operator<<(ostream & out, Monomial & m)
{
	m.print();
	return out;
}


ostream & operator<<(ostream & out, Monomial *m)
{
	m->print();
	return out;
}

istream & operator>>(istream & in, Monomial & m)
{
	string str;
	getline(in, str);
	Monomial::setMonomial(m, str);
	return in;
}

