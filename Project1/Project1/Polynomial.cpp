#include "Polynomial.h"
#include <string>
using namespace std;



bool Polynomial::isEmpty()
{
	return this->size == 0;
	Monomial::count++;
}

void Polynomial::addPoly(Polynomial & p1, Polynomial & p2)
{
	for (int i = 0; i < p2.size; i++)
		p1.add(*(p2.poly[i]));
}

Polynomial::Polynomial()
{
	this->size = 0;
	this->poly = (Monomial**)malloc(size * sizeof(Monomial*));
}

void Polynomial::add(Monomial m)
{
	for (int i = 0; i < this->size; i++) {
		if (poly[i]->degree == m.degree) {
			poly[i]->coefficient += m.coefficient;
			if (poly[i]->coefficient == 0 && poly[i]->degree!=0) {
				this->size--;
				Monomial *tmp = poly[i];
				for (int j = i; j < size - 1; j++)
					poly[j] = poly[j + 1];
				this->poly = (Monomial**)realloc(this->poly, this->size * sizeof(Monomial*));
				delete tmp;
			}
			return;
		}
	}
	/* If new degree is adding, then add one more length to the array and add the new monomial */
	this->size += 1;
	this->poly = (Monomial**)realloc(this->poly, this->size * sizeof(Monomial*));
	Monomial *tmpMonomial = new Monomial(m.coefficient, m.degree);
	poly[this->size - 1] = tmpMonomial;
}

void Polynomial::print()
{
	if (this->isEmpty()) {
		cout << "0";
		return;
	}
	Monomial::count++;
	for (int i = 0; i < this->size; i++) {
		poly[i]->print();
		/* Add '+' if not the last monomial */
		if (i != size-1) {
			cout << "+";
		}
	}
}

Polynomial::~Polynomial()
{
	//Deleting all Monomial in poly
	for (int i = 0; i < this->size; i++)
		delete poly[i];
	// Delete Poly array
	delete[] poly;
}

Polynomial & Polynomial::operator+=(const Monomial & m)
{
	m.count++;
	this->add(m);
	return *this;
}

bool Polynomial::operator==(const Monomial & m)
{
	return this->size == 1 &&
		this->poly[0]->coefficient == m.coefficient &&
		this->poly[0]->degree == m.degree;
}

bool Polynomial::operator!=(const Monomial & m)
{
	return !this->operator==(m);
}

int Polynomial::operator()(int num)
{
	if (this->isEmpty())
		return 1;
	int sum = 0;
	Monomial temp;
	for (int i = 0; i < this->size; i++) {
		temp = *(this->poly[i]);
		sum += temp(num);
	}
	return sum;
}

int Polynomial::operator[](int degree)
{
	for (int i = 0; i < this->size; i++) {
		if (this->poly[i]->degree == degree)
			return this->poly[i]->coefficient;
	}
	return 0;
}

Polynomial * Polynomial::operator+(Polynomial & p)
{
	Polynomial* poly = new Polynomial();
	addPoly(*poly, *this);
	addPoly(*poly, p);
	return poly;
}

Polynomial * Polynomial::operator-()
{	
	Monomial *temp;
	Monomial* m;
	Polynomial* poly = new Polynomial();
	for (int i = 0; i < this->size; i++) {
		temp = this->poly[i];
		m = -(*temp);
		poly->add(*m);
	}
	return poly;
}

void Polynomial::operator=(const Polynomial & poly)
{
	this->size = poly.size;
	this->poly = poly.poly;
}

ostream & operator<<(ostream & out, Polynomial & p)
{
	p.print();
	return out;
}


ostream & operator<<(ostream & out, Polynomial *p)
{
	p->print();
	return out;
}

istream & operator>>(istream & in, Polynomial & p)
{
	Polynomial* tmp = new Polynomial();
	p = *tmp;
	string str;
	Monomial* m;
	string tempMonomial;
	getline(in, str);
	for (int i = 0; i < (int)str.length(); i++) {
		if (str[i] == '+' || str[i] == '-' || str[i] == ',') {
			m = new Monomial();
			Monomial::setMonomial(*m, tempMonomial);
			p.add(*m);
			tempMonomial = str[i];
		}
		else {
			tempMonomial += str[i];
		}
	}
	return in;
}
